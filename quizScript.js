﻿
var submitButton = document.getElementById("submit");
var paspaudimai = document.getElementsByClassName("atsakymas");
var lentele1 = document.getElementById("lentele");
var x = document.getElementById("iksas");
var tekstas = document.getElementById("tekstas");
var pasirinkimai = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
var klausimai = [
    {
        klausimas: "Kuris žemynas didžiausias ?",
        atsakymai: [
            "Azija",
            "Šiaurės Amerika",
            "Antarktida"
        ],
        teisingasAtsakymas: 0,
    },
    {
        klausimas: "Kiek yra naminių šunų veislių?",
        atsakymai: [
            "Beveik tūkstantis",
            "4 tūkst.",
            "Apie 400"
        ],
        teisingasAtsakymas: 1
    },
    {
        klausimas: "Kiečiausias žemės mineralas?",
        atsakymai: [
            "Granitas",
            "Deimantas",
            "Korundas"
        ],
        teisingasAtsakymas: 1
    },
    {
        klausimas: "Kiek žmogus turi kaulų?",
        atsakymai: [
            "87",
            "151",
            "206"
        ],
        teisingasAtsakymas: 2
    },
    {
        klausimas: "Kiek planktono per dieną suėda mėlynasis banginis?",
        atsakymai: [
            "Iki pusės tonos",
            "Keturias tonas",
            "Devynias tonas"
        ],
        teisingasAtsakymas: 1
    },
    {
        klausimas: "Kuri Saulės sistemos planeta yra didžiausia?",
        atsakymai: [
            "Neptūnas",
            "Saturnas",
            "Jupiteris"
        ],
        teisingasAtsakymas: 2
    },
    {
        klausimas: "Didžiausia Egipto piramidė?",
        atsakymai: [
            "Gizos",
            "Cheopso",
            "Mikerino"
        ],
        teisingasAtsakymas: 1
    },
    {
        klausimas: "Kaip vadinamas pirmasis pasaulyje nacionalis parkas?",
        atsakymai: [
            "Mamuto urvo",
            "Ledyno nacionalis parkas",
            "Jeloustono"
        ],
        teisingasAtsakymas: 2
    },
    {
        klausimas: "Kiek pusių turi 'Heptagon' forma?",
        atsakymai: [
            "7",
            "8",
            "9"
        ],
        teisingasAtsakymas: 0
    },
    {
        klausimas: "Kokiai gyvūnų klasei priklauso vėžlys?",
        atsakymai: [
            "Žinduolių",
            "Amfibijų",
            "Roplių"
        ],
        teisingasAtsakymas: 2
    }
];

console.log(klausimai);
console.log(klausimai[0].atsakymai[2]);

function spausdintiKlausima() {
    var k = 0;
    for (var i = 0; i < klausimai.length; i++) {
        document.getElementsByClassName("klausimas")[i].innerHTML = klausimai[i].klausimas;
        for (var j = 0; j < klausimai[i].atsakymai.length; j++) {
            document.getElementsByClassName("atsakymas")[j + k].innerHTML = klausimai[i].atsakymai[j];
        }
        k += 3;
    }
}

//iskarto rodyti quiz
spausdintiKlausima();

for (var i = 0; i < paspaudimai.length; i++) {

    paspaudimai[i].addEventListener("click", function () { rinktisAtsakymus(event) });
}

function rinktisAtsakymus(e) {

    for (var i = 0; i < klausimai.length; i++) {
        if (klausimai[i].atsakymai.indexOf(e.currentTarget.innerText) > -1) {

            for (var j = 0; j < klausimai[i].atsakymai.length; j++) {
                paspaudimai[j + i * 3].classList.remove("pilka");
                console.log(j + i * 3);
            }
            pasirinkimai[i] = e.currentTarget.innerText;

            e.currentTarget.classList.add("pilka");
        }
        
    }
}

function atidaryti() {
    lentele1.classList.add("rodyti");
}

submitButton.addEventListener("click", function () {
    
    var rezultatas = 0;
    for (var i = 0; i < klausimai.length; i++) {
        for (var j = 0; j < klausimai[i].atsakymai.length; j++) {
            
            if (pasirinkimai[i]==klausimai[i].atsakymai[klausimai[i].teisingasAtsakymas]) {
                rezultatas += 1;
            } 
            j += 3;
        }
    }
    atidaryti();
    if (rezultatas <= 4) {
        
        document.getElementById("tekstas").innerText = "Surinkti taškai: " + rezultatas + " iš 10" + " Tu neesi gudresnis už šeštoką :(";
    } else if (rezultatas >= 5 && rezultatas <= 8) {
        
        document.getElementById("tekstas").innerText = "Surinkti taškai: " + rezultatas + " iš 10"  + " Tu esi pakankamai gudrus.";
    } else {
        
        document.getElementById("tekstas").innerText = "Surinkti taškai: " + rezultatas + " iš 10" + " Sveikinu, tu esi gudrenis už šeštoką!";
    }
});

function escape() {
    lentele1.classList.remove("rodyti");
}

x.addEventListener("click", function () {
    escape();
});

document.onkeydown = function (evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        escape();
    }
};



/*
function show_selected() {
   // var selector = document.getElementById('id_of_select');
    // var value = selector[selector.selectedIndex].value;

    //console.log(value);
}

*/